import {
  fetchUserToken,
  fetchUserRequest,
  fetchUserSuccess,
  fetchUserFailure,
} from './userSlice';

import axios from 'axios';

export const fetchUser = async (user, dispatch) => {
  dispatch(fetchUserRequest());
  try {
    const respond = await axios.get('/user', {
      headers: {
        Authorization: `Bearer ${dispatch(fetchUserToken(user.token))}`,
      },
    });
    dispatch(fetchUserSuccess(respond.data));
  } catch {
    const error = dispatch(fetchUserFailure(error.message));
  }
};
