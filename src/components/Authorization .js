import React from 'react';
import Loader from 'react-loader-spinner';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchUser, fetchUserToken } from '../redux';

const Authorization = ({
  userData,
  onFetchUser,
  userDataapiToken,
  tokenChange,
}) => {
  // fcBiuNs7sWzHsNzjcfr3
  return (
    <div>
      <div>
        <input
          type="text"
          // value={userData.token}
          value={userData.token}
          placeholder="API Token"
          onChange={(e) => {
            tokenChange(e.target.value);
          }}
        />
        <button
          onClick={() => {
            onFetchUser();
          }}
        >
          Get
        </button>
      </div>
      {userData.loading ? (
        <Loader
          type="Puff"
          color="#00BFFF"
          height={40}
          width={40}
          timeout={10000}
        />
      ) : userData.error ? (
        <p>{userData.error}</p>
      ) : (
        <div>
          {/* <img src={userData.users.avatar_url} alt="profile_pic" /> */}
          {/* <h2>{userData.users.name}</h2> */}
          <Link to="/project-list">Project List</Link>
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    userData: state.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onFetchUser: () => dispatch(fetchUser()),
    tokenChange: (e) => dispatch(fetchUserToken(e)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Authorization);
